import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../components/Home.vue'
import Ken from '../views/Ken.vue'
import Category from '../components/Categories.vue'
import Product from '../components/Products.vue'
import Slide from '../components/Slide.vue'
import Post from '../components/Post.vue'
import Login from '../components/Login.vue'
import Logout from '../components/Logout.vue'
import Guarantee from '../components/Guarantee.vue'
import Statistical from '../components/Statistical.vue'
Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path:'/ken',
    name:'Ken',
    component: Ken
  },
  {
    path:'/category',
    name:'category',
    component: Category
  },
  {
    path:'/product',
    name:'product',
    component: Product
  },
  {
    path:'/slide',
    name:'slide',
    component: Slide
  },
  {
    path:'/post',
    name:'post',
    component: Post
  },
  {
    path:'/login',
    name:'login',
    component: Login
  },
  {
    path:'/logout',
    name:'logout',
    component: Logout
  },
  {
    path:'/guarantee',
    name:'guarentee',
    component: Guarantee
  }
  ,{
    path:'/statistical',
    name:'statistical',
    component: Statistical
  }
  
  
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
