<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;
use DB;
class OrderController extends Controller
{
    //

    public function getAll(){
        $orders = DB::table('orders')->join('users','orders.user_id','=','users.id')->select('orders.id','users.name','users.address','orders.note','users.phone','orders.status','orders.created_at')->orderBy('id','desc')->paginate(10);
        return \response()->json([
            'status'=>true,
            'code'=>200,
            'data'=>$orders
      ]);
    }

    public function processUpdate($id,Request $request){

        $oder = Order::find($id);
        $oder->status=$request->status;
        $oder->save();
        return \response()->json([
            'status'=>true,
            'code'=>200,
            'data'=>$oder
      ]);

    }

    public function productData($id){
        $datas = DB::table('order_product')->where('order_id','=',$id)->join('products','order_product.product_id','=','products.id')
        ->select('order_product.product_id as id','order_product.product_id as id','order_product.product_name as name','order_product.qty','order_product.size','order_product.product_price as price','order_product.total','products.image as image')->get();
        
        return \response()->json([
            'status'=>true,
            'code'=>200,
            'data'=>$datas
      ]);
    }
}
