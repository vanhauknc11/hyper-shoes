<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Size;
use App\Http\Requests\ProductRequest;
use App\Http\Requests\ProductRequestUpdate;
use DB;
class ProductController extends Controller
{
    
    public function index()
    {

        $product = DB::table('products')->join('categories','products.category_id','=','categories.id')->orderBy('id','desc')
        ->select('products.id','products.name','categories.name as category','products.slug','products.price','products.image')->where('products.delete','<>',1)->paginate(15);
    //    dd($product);

      //  $product = Product::where('delete','=',0)->orderBy('id','desc')->paginate(15);
      //  dd($product);
      return \response()->json([
            'status'=>true,
            'code'=>200,
            'data'=>$product
      ]);
    }

 

    public function store(ProductRequest $request)
    {
        $product = new Product;

        $product->category_id = $request->category_id;
        $product->name = $request->name;
        $product->slug = $request->slug;
        $product->price = $request->price;

        $filename = rand(0,100000).$request->file('image')->getClientOriginalName();
        $path = $request->file('image')->move(public_path("/images"),$filename);
        $photoUrl = url('/images/'.$filename);
        $product->image = $photoUrl;
        $product->delete = 0;

        $product->save();

        return \response()->json([
            'status'=>true,
            'code'=>200,
            'data'=>[
                $product
            ]
        ]);

    }


    public function show($id)
    {
        $product = Product::find($id);
        if($product != null){

            $cate =  $product->categories;
            $images =  $product->images;
            $sizes = $product->sizes;
          
             
            return \response()->json([
                'status'=>true,
                'code'=>200,
                'data'=>[
                    'product'=>$product,
                   
                ]
            ]);

        }else{
            return \response()->json([
                'status'=>false,
                'code'=>200,
                'message'=>'Không tồn lại sản phẩm '
          ]);
        }
      
    }



    public function update(Request $request, $id)
    {
       

        $product = Product::find($id);
        if($product != null){

            if($this->checkSlug($product,$request->slug)){

                $product->category_id = $request->category_id;
                $product->name = $request->name;
                $product->slug = $request->slug;
                $product->price = $request->price;
    
                if($request->file('image')){
                $filename = rand(0,100000).$request->file('image')->getClientOriginalName();
                $path = $request->file('image')->move(public_path("/images"),$filename);
                $photoUrl = url('/images/'.$filename);
                $product->image = $photoUrl;
                }
    
                $product->delete = 0;
    
                $product->save();
    
                return \response()->json([
                    'status'=>true,
                    'code'=>200,
                    'data'=>[
                        $product
                    ]
                ]);


            }else{
                return   \response()->json([
                    'status'=>false,
                    'code' =>200,
                    'message' => "Slug đã bị trùng"
                ]);
            }

          

            

        }
        else{

            return \response()->json([
                'status'=>false,
                'code'=>200,
                'message'=>'Không tồn lại sản phẩm '
          ]);

        }
    }

    public function checkSlug($product,$slug){
       
        $temp = Product::where('id','<>',$product->id)->get();
       
        foreach($temp as $ct){
            
             if($ct->slug === $slug){
                 return false;
             }
        }
        return true;
     }


    public function destroy($id)
    {
        $product = Product::find($id);
        if($product != null){
            $product->delete=1;
            $product->save();

            return \response()->json([
                'status'=>true,
                'code'=>200
            ]);
        }else{

            return \response()->json([
                'status'=>false,
                'code'=>200,
                'message'=>'Không tồn lại sản phẩm '
          ]);

        }
    }

    public function addSize(Request $request,$id){
        
        $product = Product::find($id);
        $arrSize = $product->sizes->pluck('id')->toArray();
        if($product != null){

            foreach($request->sizes as $size){
            if( $this->checkInArray($size,$arrSize) )
            {
                $product->sizes()->attach($size);
            }
            }
            return \response()->json([
                'status'=>true,
                'code'=>200,
                'data'=>[
                    $product->sizes->pluck('id')
                ]
            ]);

        }else{
            return \response()->json([
                'status'=>false,
                'code'=>200,
                'message'=>'Không tồn lại sản phẩm '
          ]);
        }

    }

    public function checkInArray($size,$arr){
       
        for($i=0;$i<count($arr);$i++){
            if($size == $arr[$i]){
                return false;
            }
            
        }
        return true;
    }
}
