<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Services\PostService;

class PostController extends Controller
{
	protected $postService;

	public function __construct(PostService $postService)
	{
		$this->postService = $postService;
	}

    public function show($slug)
    {
    	return view('frontend.gioithieu.gioithieu', [
			'post' => $this->postService->findBySlug($slug)
			
    	]);
    }
}
