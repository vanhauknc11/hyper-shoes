<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\UserService;
use Illuminate\Support\Facades\Session;
use Auth;
use App\User;
use App\Models\Address;
use App\Models\Order;
use DB;
class HomeController extends Controller
{
    protected $userService;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserService $userService)
    {
        $this->middleware('auth');
        $this->userService = $userService;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */


    public function confirm(){
        $user =Auth::user();

      

        
        if(!$user->phone){
          //  dd('phone null');
          Session::flash('message','Bạn phải cập nhật số điện thoại');
            return \redirect()->route('user');
        }

        if(!$user->address){
            Session::flash('message','Bạn phải cập nhật sổ địa chỉ');
            return \redirect()->route('user');
        }
     
        return view('frontend.adress.adress');
    }
    public function user(){
        return view('home');
        
    }
    public function userpost(Request $request){
        $this->userService->changeInfo($request);
        return \redirect()->back(); 

    }
    public function orders(){

       $orders =  $this->userService->showHistory(30,Auth::user()->id);

        return view('frontend.user.history',[
            'orders'=>$orders,
        ]);
    }
   

    public function orderview($id){

        $od = Order::find($id);
       if($od != null){
        $products = DB::table('order_product')->where(['order_id'=>$od->id])->get();
        //  dd($products);
          return view('frontend.user.orderview',['orders'=>$od,
          'products'=>$products
          
          ]);

       } else{
        return \redirect()->back();
       }
      
    }
}
