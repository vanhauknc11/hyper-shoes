<?php

namespace App\Services;
use App\User;
use App\Models\Order;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
class PayService
{
   public function createOrder(Request $request){
    $products = Session::get('carts');


        $order = new Order;
        $order->user_id = $request->id;
        
        $order->note=$request->note;
        $order->status=0;

        $order->save();

        return $order->id;

     
   }

   public function oderDetail($id_order){

    $order = Order::find($id_order);
    $products = Session::get('carts');
    
    foreach($products as $product){

    $order->products()->attach($product->id,[
        'product_name'=>$product->name,
        'product_price'=>$product->price,
        'qty'=>$product->qty,
        'size'=>$product->size,
        'total'=>$product->price*$product->qty
    ]);
    }
    return true;
   }

}