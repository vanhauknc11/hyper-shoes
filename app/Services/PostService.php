<?php

namespace App\Services;

use App\Models\Post;

class PostService
{
	public function findBySlug($slug)
	{
		return Post::where(['slug' => $slug])->first();
	}

	public function getPost(){
		return Post::orderBy('id')->take(10)->get();
	}
}