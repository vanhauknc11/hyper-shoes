<?php

namespace App\Services;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;
use DB;
class UserService
{
    public function changeInfo(Request $request){

    
        $user = User::where(['email'=>$request->email])->first();
        if(!$request->checkbox){
            $user->phone = $request->phone;
            $user->address = $request->address;
            $user->name=$request->name;
            $user->save();
            Session::flash('message','Cập nhật thành công !');
        }else{
            
        if( strlen ($request->password)<6 || strlen ($request->oldpassword )<6 || strlen ($request->repassword)<6){
            Session::flash('message','Mật khẩu phải dài hơn 6 ký tự');
            return;
        }
           

            $user->name=$request->name;
            if($request->password != $request->repassword){
           
                Session::flash('message','2 mật khẩu không khớp');
                return;
            }
            if(Hash::check($request->oldpassword,$user->password)){
             
                $user->name=$request->name;
                $user->phone = $request->phone;
                $user->address = $request->address;
                $user->password = Hash::make($request->password );
                $user->save();
                Session::flash('message','Cập nhật thông tin thành công ');
                return;
               
            }else{
                
                Session::flash('message','Mật khẩu cũ không chính xác');
                return;
               
            }
         
        }
        
    }


    public function showHistory($limit=30,$user_id){

        $user = DB::table('order_product')
        ->join('orders','order_product.order_id','=','orders.id')->
        select('order_product.order_id','orders.created_at',DB::raw('count(order_product.order_id) as SoSanPham'),DB::raw('SUM(total) as TongTien'),'orders.status')
        ->where(['orders.user_id'=>$user_id])
        ->groupBy('order_product.order_id')
        ->orderBy('order_product.order_id','desc')
        ->paginate($limit);
   //     dd($user);
     return $user;
    }
}