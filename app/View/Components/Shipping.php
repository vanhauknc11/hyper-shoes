<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Services\CategoryService;

class Shipping extends Component
{
    public $freeShipContent = 'Cam kết sản phẩm chính hãng từ Châu Âu, Châu mỹ... ';

    public $freeReturnContent = 'SHIP hỏa tốc 1h nhận hàng trong nội thành HCM';

    public $secureContent = 'Gọi ngay 0962938734';

    public $price;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($price = '', $name = null)
    {
        $this->price = $price;
        $this->freeShipContent  .= '$' . "{$price}";
        $this->secureContent .= $name;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.shipping');
    }
}
