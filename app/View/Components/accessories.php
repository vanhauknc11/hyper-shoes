<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Services\ProductService;
class accessories extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */

    public $limit;
    public $pk;
    public function __construct($limit,ProductService $categorySer)
    {
        //
         $this ->pk = $categorySer->productMin($limit);
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.accessories');
    }
}
