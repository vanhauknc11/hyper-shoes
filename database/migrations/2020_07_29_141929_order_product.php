<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class OrderProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('order_product', function (Blueprint $table) {

            $table->unsignedInteger('product_id');
            $table->unsignedInteger('order_id');
            $table->string('product_name',255);
            $table->tinyInteger('qty');
            $table->tinyInteger('size');
            $table->float('product_price');
            $table->float('total');
    
            $table->index(['product_id', 'order_id']);
            
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_product');
    }
}
