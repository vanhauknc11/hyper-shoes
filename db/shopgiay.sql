-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 26, 2020 at 01:55 AM
-- Server version: 5.7.24
-- PHP Version: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shopgiay`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `delete` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`, `delete`) VALUES
(1, 'Adidas', 'adidas', 0),
(2, 'Nike', 'nike', 0),
(3, 'Yeezy', 'yeezy', 0),
(4, 'Jordan', 'jordan', 0),
(5, 'Balenciaga', 'balenciaga', 0),
(6, 'Converse', 'converse', 0),
(7, 'Vans', 'vans', 0),
(8, 'Phụ kiện', 'phu-kien', 0),
(9, 'Chanh Dat', 'chanh-dat', 1);

-- --------------------------------------------------------

--
-- Table structure for table `guarantee`
--

CREATE TABLE `guarantee` (
  `id` int(10) NOT NULL,
  `product_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_user` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_type` int(10) NOT NULL,
  `time_to_done` timestamp NOT NULL,
  `created_at` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `guarantee`
--

INSERT INTO `guarantee` (`id`, `product_name`, `email_user`, `id_type`, `time_to_done`, `created_at`) VALUES
(1, 'Giay Jordan', 'nevermorethuan1408@gmail.com', 1, '2020-08-06 03:54:16', '2020-08-13 03:54:16'),
(2, 'Giay Nike', 'nevermorethuan1408@gmail.com', 2, '2020-08-24 07:32:17', '2020-08-27 07:32:17'),
(7, 'Giay A', 'nevermorethuan1408@gmail.com', 2, '2020-08-29 06:15:38', '2020-08-24 06:15:38'),
(8, 'GIAY TEST', 'nevermorethuan1408@gmail.com', 1, '2020-08-27 06:17:05', '2020-08-24 06:17:05');

-- --------------------------------------------------------

--
-- Table structure for table `guarantee_type`
--

CREATE TABLE `guarantee_type` (
  `id` int(10) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `guarantee_type`
--

INSERT INTO `guarantee_type` (`id`, `name`, `time`) VALUES
(1, 'Đế giày bị hỏng', 3),
(2, 'Giày bị lỗi', 5);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(11, '2020_07_27_101839_create_addresses_table', 1),
(92, '2014_10_12_100000_create_password_resets_table', 2),
(93, '2020_06_18_130729_create_categories_table', 2),
(94, '2020_06_18_131038_create_products_table', 2),
(95, '2020_06_18_131408_create_product_images_table', 2),
(96, '2020_06_18_134350_create_order_table', 2),
(97, '2020_07_02_133527_create_posts_table', 2),
(98, '2020_07_15_084345_create_slides_table', 2),
(99, '2020_07_20_164626_product_size', 2),
(100, '2020_07_20_171552_create_sizes_table', 2),
(101, '2020_07_22_160148_create_users_table', 2),
(102, '2020_07_29_141929_order_product', 2);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `note` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `note`, `status`, `created_at`, `updated_at`) VALUES
(3, 1, 'this is note', 1, '2020-08-22 09:46:36', '2020-08-23 10:19:43'),
(4, 1, 'note 2', 0, '2020-08-22 09:52:01', '2020-08-22 09:52:01'),
(7, 1, 'note 3', 1, '2020-08-22 09:54:55', '2020-08-23 09:40:02'),
(8, 1, NULL, 1, '2020-08-22 10:53:48', '2020-08-23 09:39:56'),
(9, 2, NULL, 1, '2020-08-23 00:20:30', '2020-08-23 10:21:34'),
(10, 1, '2 san pham', 1, '2020-08-23 10:07:52', '2020-08-23 10:19:57');

-- --------------------------------------------------------

--
-- Table structure for table `order_product`
--

CREATE TABLE `order_product` (
  `product_id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qty` tinyint(4) NOT NULL,
  `size` tinyint(4) NOT NULL,
  `product_price` double(11,2) NOT NULL,
  `total` double(11,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_product`
--

INSERT INTO `order_product` (`product_id`, `order_id`, `product_name`, `qty`, `size`, `product_price`, `total`) VALUES
(22, 3, 'MLB YANKEE BOSTON RED SOX', 1, 37, 2250000.00, 2250000.00),
(16, 4, 'JORDAN 1 HIGH GYM RED', 1, 40, 9500000.00, 9500000.00),
(9, 4, 'YEEZY BOOST 700 MAGNET', 2, 42, 9900000.00, 19800000.00),
(21, 7, 'MLB YANKEES BLACK', 1, 38, 2250000.00, 2250000.00),
(19, 7, 'BALENCIAGA TRACK TRAINERS ORANGE', 2, 38, 2200000.00, 4400000.00),
(15, 8, 'JORDAN 1 LOW BLACK TOE', 2, 41, 6200000.00, 12400000.00),
(11, 9, 'YEEZY BOST 350 V2 BLACK', 1, 40, 2000000.00, 2000000.00),
(22, 10, 'MLB YANKEE BOSTON RED SOX', 2, 37, 2250000.00, 4500000.00),
(5, 10, 'AIR FORCE 1 SHADOW SAPPHIRE', 2, 39, 3900000.00, 7800000.00);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `slug`, `image`, `content`, `created_at`, `updated_at`) VALUES
(4, 'Tại sao mẫu giày EQT Support ADV khiến ai cũng muốn sở hữu nó ?', 'tai-sao-lai-muon-co-eqt', 'http://hypershoes.test/images/79471adidaseqtequipmentlockupsizehqexclusive19small-6666.jpg', 'Ngoài \"Những gì thiết yếu nhất, không gì là thừa thãi\", bạn còn nhận được vô số giá trị khi sở hữu dòng giày adidas EQT 2017.\n\nCó rất nhiều lý do khiến bạn \"thu nạp\" thêm giày mới vào tủ đồ của mình: bạn là một người tiêu dùng (consumer) thuần túy, mua giày theo nhu cầu, dùng đến khi hỏng rồi thay đôi khác hoặc bạn là người yêu giày, thích sưu tập giày, mua cả tá về trưng hoặc thậm chí kiếm tiền từ việc bán giày.\n\nNếu còn đang \"loanh quanh\" với cả tá thương hiệu giày, không biết chọn mua gì. Tôi sẽ cho bạn biết 5 lý do không thể bỏ qua adidas EQT nếu muốn mua giày mới ngay lúc này.', '2020-08-23 00:03:16', '2020-08-23 00:03:16'),
(5, 'Mẫu giày MLB làm dậy sóng giới trẻ Hàn Quốc và Thế Giới !!!', 'mau-giay-mlb-lam-day-song', 'http://hypershoes.test/images/30356668365343993949041043414687742797184502577n-1004.jpg', 'Giữa những đôi giày theo xu hướng chunky shoes đến từ những thương hiệu giày nổi tiếng, MLB vẫn tạo nên nét riêng cho mình. Những đôi giày MLB Big Ball khiến những tín đồ không chỉ của Hàn Quốc mà trên toàn thế giới chao đảo với những mẫu logo vô cùng đơn giản. Vậy điều gì đã khiến MLB được yêu mến đến vậy.\n\nHãy cùng Anonymousstore.vn tìm hiểu nhé!!!\n\n \n\nMLB là ai?\n\nMajor League Baseball (MLB) là tổ chức thể thao chuyên nghiệp của môn bóng chày và cũng là tổ chức lâu đời nhất trong 4 liên đoàn thể thao chuyên nghiệp chính ở Hoa Kỳ và Canada. Với tuổi đời hơn 100 năm (từ năm 1901), Major League Baseball gồm 30 đội bóng đến từ nhiều bang khác nhau của Mỹ và Canada, trong đó 29 đội đến từ Mỹ và 1 đội đến từ Canada. Biểu tượng chính của giải đấu là một người đang trong thế đánh bóng chày – biểu tượng dựa trên hình tượng của Jackie Robinson. Jackie Robinson là một trong những cầu thủ huyền thoại và là người da màu đầu tiên chơi cho giải.', '2020-08-23 00:04:21', '2020-08-23 00:04:21'),
(6, 'Những công nghệ làm thay đổi nền công nghiệp Sneaker thế kỷ 21', 'nhung-cong-nghe-lam-thay-doi-nen-cong', 'http://hypershoes.test/images/8101dinhnghiagiaysneakerlagi2-3181.jpg', 'Nike, adidas, Reebok và Asics được cho là những người khổng lồ cống hiến nhiều nhất cho nền văn hóa \"sát mặt đất.\".\n\nCông nghệ đột phá nhất làng sneakers thế giới, từng thuộc về bộ đế waffle của Nike vào những năm 1970s. Đến nay, chúng ta đã có sneakers in 4D và công nghệ tự động thắt dây (auto-lacing).\n\nTheo Highsnobiety, một trong những công nghệ sneakers đột phá nhất trong lịch sử đến từ những ông lớn trong ngành công nghiệp sportwear, bao gồm Nike, adidas, Reebok và Asics.\n\nCùng với sự chuyển mình của công nghệ, nền văn hóa \"sát mặt đất\" cũng được hưởng lợi và có nhiều đột phá về bộ đệm cũng như được tích hợp linh kiện điện tử. Cùng khám phá 11 công nghệ sneakers đã góp phần tạo nên ngành công nghiệp giày thể thao của thế kỷ 21:', '2020-08-23 00:05:17', '2020-08-23 00:05:17'),
(7, 'MẸO VẶT TRỊ MÙI HÔI KHÓ CHỊU KHI MANG GIÀY', 'me-vat-tri-mui-hoi-kho-chiu', 'http://hypershoes.test/images/19956meovattrimuihoikhochiukhimanggiay-2721.jpeg', 'Giày có mùi khó chịu do chính mồ hôi bàn chân của bạn gây ra. Đó là điều ko ai thích vì thế các bạn có thể áp dụng các bước sau đây :\n\n\n1.Trước khi mang giày nên lấy 1 ít phấn rôm em bé thoa lên bàn chân mình sau đó hãy mang vớ (tất) vào:', '2020-08-23 00:06:52', '2020-08-23 00:06:52'),
(8, 'Vệ sinh giày đúng cách như thế nào ?', 've-sinh-giay-dung-cach', 'http://hypershoes.test/images/83991d55332f359aebff0e6bf-2332.jpg', '- Dung dịch vệ sinh giày Q-Shine\n\n- Bàn chải giày ( lông mềm ), bàn chải đánh răng\n\n- 1 vài chiếc khăn sạch\n\nAnonymous sẽ hướng dẫn bạn từng bước vệ sinh sneaker tuần tự để các bạn có thể dễ dàng làm theo nhé. Các bước cần để làm sạch sneaker gồm:\n\n- Kiểm tra\n\n- Vệ sinh dây giày\n\n- Vệ sinh đế giày\n\n- Vệ sinh thân giày\n\n- Vệ sinh lót giày (nếu cần)\n\n- Hong khô\n\n1. Kiểm tra vế', '2020-08-23 00:07:36', '2020-08-23 00:07:36'),
(9, 'Công nghệ Nike React là gì ?', 'cong-nghe-react', 'http://hypershoes.test/images/948517d1b5ab088e26ebc37f3-1500.jpg', 'Nike React chính là một trong những công nghệ đế giày thể thao nổi bật nhất 2018. Nike miêu tả Nike React một cách dễ hiểu bằng có so sánh độ đàn hồi và êm ái của những chiếc gối nằm. Nhưng để dễ hơn, bạn cảm thấy thế nào nếu mang dép đi trên những tấm thảm tại khách sạn 5 sao? Rất êm và không cần tốt nhiều sức, đúng không? Nike React được sáng tạo hướng đến mục đích đó.\n\nNike React giúp chuyển hướng và tăng tốc tốt hơn khi áp dụng vào các dòng giày bóng rổ. Điều này cực kỳ quan trọng cho người chơi bóng rổ, lẫn chiến lược giữ vững vị trí đầu bảng của những đôi giày bóng rổ Nike trong suốt thập kỷ vừa qua.\n\nNếu so sánh giày Nike Epic React Flyknit với Nike LunarEpic 2 Flyknit , cả hai có cùng chất liệu vải Flyknit và thiết kế tương tự của dòng Epic, nhưng đế giày Nike React giúp Epic React Flyknit nhẹ hơn 5% so với với mẫu giày không sử dụng đế React. Nhưng nếu so với các mẫu giày chạy bộ khác thì Nike React Flyknit nhẹ hơn đến 30%, bởi đế giày React và vải Flyknit . Nếu so về độ đàn hồi, Nike React đàn hồi tốt hơn 11%.', '2020-08-23 00:08:29', '2020-08-23 00:08:29');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(11,2) NOT NULL DEFAULT '0.00',
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `delete` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `category_id`, `name`, `slug`, `price`, `image`, `delete`) VALUES
(1, 1, 'SUPERSTART', 'superstart', 1950000.00, 'http://hypershoes.test/images/67713ca.jpg', 0),
(2, 1, 'STAN SMITH WHITE HOLOGRAM', 'stan-smith-hologram', 2200000.00, 'http://hypershoes.test/images/10983caz.jpg', 0),
(3, 1, 'STAN SMITH GREEN', 'stan-smith-green', 1950000.00, 'http://hypershoes.test/images/89807bd.jpg', 0),
(4, 2, 'AIR FORCE 1', 'air-force-one', 2500000.00, 'http://hypershoes.test/images/14011vcxxvcwe3.jpg', 0),
(5, 2, 'AIR FORCE 1 SHADOW SAPPHIRE', 'air-force-1-shadow-sapphire', 3900000.00, 'http://hypershoes.test/images/85579cj1641-1001.jpg', 0),
(6, 2, 'ADAPT BB DARK GREY', 'adapt-bb-dark-grey', 9600000.00, 'http://hypershoes.test/images/56323ao2582-0042.jpg', 0),
(7, 2, 'BLAZER MID SKETCH SWOOSH', 'blazer-mid-sketch-swoosh', 3800000.00, 'http://hypershoes.test/images/8486cw7580-1011.jpg', 0),
(8, 3, 'YEEZY BOOST 350 V2 ZEBRA', 'yeezy-boost-350', 9500000.00, 'http://hypershoes.test/images/59710xcvxv3.jpg', 0),
(9, 3, 'YEEZY BOOST 700 MAGNET', 'yeezy-boost-700-magnet', 9900000.00, 'http://hypershoes.test/images/61980fv9922.jpg', 0),
(10, 3, 'YEEZY BOOST 350 V2 CREAM WHITE', 'yeezy-boost-350-v2', 11000000.00, 'http://hypershoes.test/images/30826bbajw2.jpg', 0),
(11, 3, 'YEEZY BOST 350 V2 BLACK', 'yeezy-bost-350-v2-black', 2000000.00, 'http://hypershoes.test/images/38907adidas-yeezy-350-v2-black-fu9006-2-1560832255-.jpg', 0),
(12, 4, 'JORDAN 1 LOW MULTI-COLOR', 'jordan-1-1low-multi', 4800000.00, 'http://hypershoes.test/images/95579cz4776-1011.jpg', 0),
(13, 4, 'JORDAN 1 LOW NOBLE RED', 'jordan-1-low-noble-red', 3900000.00, 'http://hypershoes.test/images/76087123395.jpg', 0),
(14, 4, 'JORDAN 1 LOW NOTHING BUT NET', 'nothing-but-net', 4800000.00, 'http://hypershoes.test/images/89299cz8659-1001.jpeg', 0),
(15, 4, 'JORDAN 1 LOW BLACK TOE', 'low-black-toe', 6200000.00, 'http://hypershoes.test/images/64616dfsdsfs3.jpg', 0),
(16, 4, 'JORDAN 1 HIGH GYM RED', 'jordan-1-high-gym-red', 9500000.00, 'http://hypershoes.test/images/65353555088-061.jpg', 0),
(17, 4, 'JORDAN 1 HIGH ZOOM RACER BLUE', 'high-zoom-racer-blue', 13500000.00, 'http://hypershoes.test/images/46354ck6637-104nikeairjordan1hizoomairracerblueaa.jpg', 0),
(18, 4, 'AIR JORDAN 1 HIGH OC CHIGAGO', 'air-jordan-1-high-oc-chigago', 7800000.00, 'http://hypershoes.test/images/35754nike-wmns-air-jordan-1-high-og-black-cd0461-046-6.jpg', 0),
(19, 5, 'BALENCIAGA TRACK TRAINERS ORANGE', 'balenciaga-track-trainers-orange', 2200000.00, 'http://hypershoes.test/images/56469balen_1-1565092636-.jpg', 0),
(20, 5, 'MLB YANKEES CREAM WHITE NY', 'mlb-yankees-scream-white-ny', 2250000.00, 'http://hypershoes.test/images/91947mlb_13.jpg', 0),
(21, 5, 'MLB YANKEES BLACK', 'mlb-yankees-black', 2250000.00, 'http://hypershoes.test/images/7817832shc191b.jpg', 0),
(22, 5, 'MLB YANKEE BOSTON RED SOX', 'mlb-yankee-boston-red-sox', 2250000.00, 'http://hypershoes.test/images/83593mlb-boston1.jpg', 0),
(23, 4, 'JORDAN 1 HIGH REACT', 'jordan-1-high-react', 5800000.00, 'http://hypershoes.test/images/19501ar5321-0061.jpg', 0),
(24, 4, 'JORDAN 1 MID LASER ORANGE BLACK', 'jordan-1-mid-laser-orange-black', 6800000.00, 'http://hypershoes.test/images/35302cv5276-1071.jpg', 0),
(25, 4, 'JORDAN 1 RETRO HIGH ZOOM \'FEARLESS\'', 'jordan-1-retro-high-zoom', 9500000.00, 'http://hypershoes.test/images/66949bv0006-9001.jpg', 0),
(26, 4, 'JORDAN 1 MID \'PINK AURORA GREEN\'', 'jordan-1-mid-pink', 4800000.00, 'http://hypershoes.test/images/21133555112-1022.jpg', 0),
(27, 4, 'JORDAN 1 MID UNION ROYAL', 'jordan-1-mid-union-royal', 5800000.00, 'http://hypershoes.test/images/78579852542-1021.jpg', 0),
(28, 4, 'JORDAN 1 MID AMARILLO ORANGE', 'jordan-1-mid-amarillo-orange', 4800000.00, 'http://hypershoes.test/images/96347852542-0871.jpg', 0),
(29, 4, 'JORDAN 1 MID \'CHICAGO WHITE TOE\'', 'jordan-1-mid-chacago', 4800000.00, 'http://hypershoes.test/images/15764554725-1731.jpg', 0),
(30, 4, 'JORDAN 1 MID DYNAMIC YELLOW FLORAL', 'jordan-1-mid-dynamic-yellow', 4500000.00, 'http://hypershoes.test/images/55358av5174-7001.jpg', 0),
(31, 4, 'AIR JORDAN 1 HIGH OG COURT PURPLE', 'air-jordan-1-high-og-court-purple', 9500000.00, 'http://hypershoes.test/images/35736555088-5011.jpg', 0),
(32, 4, 'JORDAN 1 MID \'SHATTERED BACKBOARD\'', 'jordan-1-mid-shttered', 4600000.00, 'http://hypershoes.test/images/35092554724-0581.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`id`, `image`, `product_id`) VALUES
(1, 'http://hypershoes.test/images/69731.jpg', 1),
(2, 'http://hypershoes.test/images/728583aw.jpg', 1),
(3, 'http://hypershoes.test/images/1128c2.jpg', 1),
(4, 'http://hypershoes.test/images/86954vvs.jpg', 1),
(5, 'http://hypershoes.test/images/736853df.jpg', 2),
(6, 'http://hypershoes.test/images/2234732f.jpg', 2),
(7, 'http://hypershoes.test/images/97692dc32.jpg', 2),
(8, 'http://hypershoes.test/images/31522uurt4.jpg', 2),
(9, 'http://hypershoes.test/images/8378523fsd.jpg', 3),
(10, 'http://hypershoes.test/images/28404fsv42.jpg', 3),
(11, 'http://hypershoes.test/images/43561sadc3.jpg', 3),
(12, 'http://hypershoes.test/images/74499v4g.jpg', 3),
(13, 'http://hypershoes.test/images/9411823fsd2.jpg', 4),
(14, 'http://hypershoes.test/images/64335asd32v.jpg', 4),
(15, 'http://hypershoes.test/images/82643v3r2bbc.jpg', 4),
(16, 'http://hypershoes.test/images/86901vcxxvcwe3.jpg', 4),
(17, 'http://hypershoes.test/images/45377cj1641-1001.jpg', 5),
(18, 'http://hypershoes.test/images/87815cj1641-1002.jpeg', 5),
(19, 'http://hypershoes.test/images/72548cj1641-1003.jpg', 5),
(20, 'http://hypershoes.test/images/13694cj1641-1004.jpg', 5),
(21, 'http://hypershoes.test/images/83918ao2582-0041.jpg', 6),
(22, 'http://hypershoes.test/images/27075ao2582-0042.jpg', 6),
(23, 'http://hypershoes.test/images/26750ao2582-0043.jpg', 6),
(24, 'http://hypershoes.test/images/76459ao2582-0044.jpg', 6),
(25, 'http://hypershoes.test/images/48485cw7580-1011.jpg', 7),
(26, 'http://hypershoes.test/images/99120cw7580-1012.jpg', 7),
(27, 'http://hypershoes.test/images/59981cw7580-1013.jpg', 7),
(28, 'http://hypershoes.test/images/54413cw7580-1014.jpg', 7),
(29, 'http://hypershoes.test/images/612688yuj.jpg', 8),
(30, 'http://hypershoes.test/images/4863932rwer32.jpg', 8),
(31, 'http://hypershoes.test/images/36622dfdf4.jpg', 8),
(32, 'http://hypershoes.test/images/48646xcvxv3.jpg', 8),
(33, 'http://hypershoes.test/images/72591fv9922.jpg', 9),
(34, 'http://hypershoes.test/images/51887fv99222.jpg', 9),
(35, 'http://hypershoes.test/images/7792fv99223.jpg', 9),
(36, 'http://hypershoes.test/images/28671fv99227.jpg', 9),
(37, 'http://hypershoes.test/images/19674bbajw2.jpg', 10),
(38, 'http://hypershoes.test/images/26603d3ds.jpg', 10),
(39, 'http://hypershoes.test/images/21615gdf43ff.jpg', 10),
(40, 'http://hypershoes.test/images/14682rwrws2.jpg', 10),
(41, 'http://hypershoes.test/images/5892761844251_375677786396101_2511217894173913857_n.jpg', 11),
(42, 'http://hypershoes.test/images/46277adidas-yeezy-350-v2-black-fu9006-2-1560832255-.jpg', 11),
(43, 'http://hypershoes.test/images/78460adidas-yeezy-350-v2-black-fu9006-4-1560742477-.jpg', 11),
(44, 'http://hypershoes.test/images/32684Yeezy-Boost-350-V2-FU9006-Black_7-1300x1300.jpg', 11),
(45, 'http://hypershoes.test/images/352cz4776-1011.jpg', 12),
(46, 'http://hypershoes.test/images/44532cz4776-1012.jpg', 12),
(47, 'http://hypershoes.test/images/81754cz4776-1014.jpg', 12),
(48, 'http://hypershoes.test/images/95114cz4776-1015.jpg', 12),
(49, 'http://hypershoes.test/images/97183123395.jpg', 13),
(50, 'http://hypershoes.test/images/27178123396.jpg', 13),
(51, 'http://hypershoes.test/images/11693emwgj9pxyauobd.jpg', 13),
(52, 'http://hypershoes.test/images/50618ennyrh1xuaidl0r.jpg', 13),
(53, 'http://hypershoes.test/images/98158cz8659-1001.jpeg', 14),
(54, 'http://hypershoes.test/images/41010cz8659-1002.jpeg', 14),
(55, 'http://hypershoes.test/images/61480cz8659-1003.jpeg', 14),
(56, 'http://hypershoes.test/images/14388cz8659-1004.jpeg', 14),
(57, 'http://hypershoes.test/images/70052535560116_3.jpeg', 15),
(58, 'http://hypershoes.test/images/26822dfsdsfs3.jpg', 15),
(59, 'http://hypershoes.test/images/51343fd3dfsdf.jpeg', 15),
(60, 'http://hypershoes.test/images/33051rrrrrrrr33d.jpg', 15),
(61, 'http://hypershoes.test/images/9897555088-061.jpg', 16),
(62, 'http://hypershoes.test/images/65881555088-061_1.jpg', 16),
(63, 'http://hypershoes.test/images/74298555088-061_2.jpg', 16),
(64, 'http://hypershoes.test/images/76304555088-061_8.jpg', 16),
(65, 'http://hypershoes.test/images/6085ck6637-104nikeairjordan1hizoomairracerblueaa.jpg', 17),
(66, 'http://hypershoes.test/images/3100ck6637-104nikeairjordan1hizoomairracerbluedd.jpg', 17),
(67, 'http://hypershoes.test/images/21191ck6637-1043.jpg', 17),
(68, 'http://hypershoes.test/images/48303ck6637-1044.jpg', 17),
(69, 'http://hypershoes.test/images/94928nike-wmns-air-jordan-1-high-og-black-cd0461-046-1.jpg', 18),
(70, 'http://hypershoes.test/images/16430nike-wmns-air-jordan-1-high-og-black-cd0461-046-2.jpg', 18),
(71, 'http://hypershoes.test/images/80791nike-wmns-air-jordan-1-high-og-black-cd0461-046-3.jpg', 18),
(72, 'http://hypershoes.test/images/31820nike-wmns-air-jordan-1-high-og-black-cd0461-046-6.jpg', 18),
(73, 'http://hypershoes.test/images/321balen_1-1565092636-.jpg', 19),
(74, 'http://hypershoes.test/images/54529balen_2-1565092636-.jpg', 19),
(75, 'http://hypershoes.test/images/62299balen_3-1565092636-.jpg', 19),
(76, 'http://hypershoes.test/images/59023balen_4-1565092636-.jpg', 19),
(77, 'http://hypershoes.test/images/56581mlb_7.jpg', 20),
(78, 'http://hypershoes.test/images/84mlb_9.png', 20),
(79, 'http://hypershoes.test/images/98629mlb_11.jpg', 20),
(80, 'http://hypershoes.test/images/34023mlb_13.jpg', 20),
(81, 'http://hypershoes.test/images/4697632shc191b.jpg', 21),
(82, 'http://hypershoes.test/images/59332shc191b2.jpg', 21),
(83, 'http://hypershoes.test/images/3194332shc191b3.jpg', 21),
(84, 'http://hypershoes.test/images/3390232shc191b6.jpg', 21),
(85, 'http://hypershoes.test/images/3367mlb_1.jpg', 22),
(86, 'http://hypershoes.test/images/9216mlb_3.jpg', 22),
(87, 'http://hypershoes.test/images/49108mlb-boston1.jpg', 22),
(88, 'http://hypershoes.test/images/89269mlb-boston4.jpg', 22),
(89, 'http://hypershoes.test/images/79508ar5321-0061.jpg', 23),
(90, 'http://hypershoes.test/images/74756ar5321-0062.jpg', 23),
(91, 'http://hypershoes.test/images/94406ar5321-0063.jpg', 23),
(92, 'http://hypershoes.test/images/19618ar5321-0067.jpg', 23),
(93, 'http://hypershoes.test/images/4947cv5276-1071.1.jpg', 24),
(94, 'http://hypershoes.test/images/98846cv5276-1071.jpg', 24),
(95, 'http://hypershoes.test/images/20954cv5276-1072.jpg', 24),
(96, 'http://hypershoes.test/images/62856cv5276-1076.jpg', 24),
(97, 'http://hypershoes.test/images/29539bv0006-9001.jpg', 25),
(98, 'http://hypershoes.test/images/97849bv0006-9002.jpg', 25),
(99, 'http://hypershoes.test/images/89940bv0006-9003.jpg', 25),
(100, 'http://hypershoes.test/images/91831bv0006-9004.jpg', 25),
(101, 'http://hypershoes.test/images/86507555112-1022.jpg', 26),
(102, 'http://hypershoes.test/images/28684555112-1023.jpg', 26),
(103, 'http://hypershoes.test/images/93267555112-1024.jpg', 26),
(104, 'http://hypershoes.test/images/30813jordan-1-pink-green.jpg', 26),
(105, 'http://hypershoes.test/images/38059852542-1021.jpg', 27),
(106, 'http://hypershoes.test/images/63314852542-1022.jpg', 27),
(107, 'http://hypershoes.test/images/8880852542-1023.jpg', 27),
(108, 'http://hypershoes.test/images/92091852542-1024.jpg', 27),
(109, 'http://hypershoes.test/images/63422852542-0871.jpg', 28),
(110, 'http://hypershoes.test/images/40912852542-0873.jpg', 28),
(111, 'http://hypershoes.test/images/71661852542-0874.jpg', 28),
(112, 'http://hypershoes.test/images/90079852542-0875.jpg', 28),
(117, 'http://hypershoes.test/images/42549554725-1731.jpg', 29),
(118, 'http://hypershoes.test/images/94309554725-1732.jpg', 29),
(119, 'http://hypershoes.test/images/48424554725-1733.jpg', 29),
(120, 'http://hypershoes.test/images/73939554725-1734.jpg', 29),
(121, 'http://hypershoes.test/images/19148av5174-7001.jpg', 30),
(122, 'http://hypershoes.test/images/57875av5174-7002.jpg', 30),
(123, 'http://hypershoes.test/images/62034av5174-7003.jpg', 30),
(124, 'http://hypershoes.test/images/24174av5174-7004.jpg', 30),
(125, 'http://hypershoes.test/images/11211555088-5011.jpg', 31),
(126, 'http://hypershoes.test/images/80812555088-5012.jpg', 31),
(127, 'http://hypershoes.test/images/2119555088-5014.jpg', 31),
(128, 'http://hypershoes.test/images/74176555088-5015.jpg', 31),
(129, 'http://hypershoes.test/images/43133554724-0581.jpg', 32),
(130, 'http://hypershoes.test/images/85559554724-0582.jpg', 32),
(131, 'http://hypershoes.test/images/76895554724-0583.jpg', 32),
(132, 'http://hypershoes.test/images/82960554724-0584.jpg', 32);

-- --------------------------------------------------------

--
-- Table structure for table `product_size`
--

CREATE TABLE `product_size` (
  `product_id` int(10) UNSIGNED NOT NULL,
  `size_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_size`
--

INSERT INTO `product_size` (`product_id`, `size_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(1, 9),
(1, 10),
(2, 1),
(2, 2),
(2, 3),
(2, 4),
(2, 5),
(2, 6),
(2, 7),
(3, 1),
(3, 2),
(3, 3),
(3, 4),
(3, 5),
(3, 6),
(3, 7),
(4, 1),
(4, 2),
(4, 3),
(4, 4),
(4, 5),
(4, 6),
(5, 2),
(5, 3),
(5, 4),
(5, 5),
(5, 6),
(6, 5),
(6, 6),
(6, 7),
(6, 8),
(6, 9),
(6, 10),
(7, 5),
(7, 6),
(7, 7),
(7, 8),
(7, 9),
(7, 10),
(8, 3),
(8, 4),
(8, 5),
(8, 6),
(8, 7),
(8, 8),
(8, 9),
(9, 3),
(9, 4),
(9, 5),
(9, 6),
(9, 7),
(9, 8),
(9, 9),
(10, 3),
(10, 4),
(10, 5),
(10, 6),
(10, 7),
(10, 8),
(10, 9),
(11, 3),
(11, 4),
(11, 5),
(11, 6),
(11, 7),
(11, 8),
(11, 9),
(12, 2),
(12, 3),
(12, 4),
(12, 5),
(12, 6),
(12, 7),
(12, 8),
(13, 2),
(13, 3),
(13, 4),
(13, 5),
(13, 6),
(13, 7),
(13, 8),
(14, 2),
(14, 3),
(14, 4),
(14, 5),
(14, 6),
(14, 7),
(14, 8),
(15, 2),
(15, 3),
(15, 4),
(15, 5),
(15, 6),
(15, 7),
(15, 8),
(16, 2),
(16, 3),
(16, 4),
(16, 5),
(16, 6),
(16, 7),
(16, 8),
(17, 2),
(17, 3),
(17, 4),
(17, 5),
(17, 6),
(17, 7),
(17, 8),
(18, 2),
(18, 3),
(18, 4),
(18, 5),
(18, 6),
(18, 7),
(18, 8),
(19, 2),
(19, 3),
(19, 4),
(19, 5),
(19, 6),
(19, 7),
(19, 8),
(20, 2),
(20, 3),
(20, 4),
(20, 5),
(20, 6),
(20, 7),
(20, 8),
(20, 9),
(21, 2),
(21, 3),
(21, 4),
(21, 5),
(21, 6),
(21, 7),
(21, 8),
(22, 2),
(22, 3),
(22, 4),
(22, 5),
(22, 6),
(22, 7),
(22, 8),
(23, 2),
(23, 3),
(23, 4),
(23, 5),
(23, 6),
(23, 7),
(23, 8),
(24, 2),
(24, 3),
(24, 4),
(24, 5),
(24, 6),
(24, 7),
(24, 8),
(25, 2),
(25, 3),
(25, 4),
(25, 5),
(25, 6),
(25, 7),
(25, 8),
(26, 2),
(26, 3),
(26, 4),
(26, 5),
(26, 6),
(26, 7),
(26, 8),
(27, 3),
(27, 4),
(27, 5),
(27, 6),
(27, 7),
(27, 8),
(28, 2),
(28, 3),
(28, 4),
(28, 5),
(28, 6),
(28, 7),
(28, 8),
(29, 3),
(29, 4),
(29, 5),
(29, 6),
(29, 7),
(29, 8),
(30, 3),
(30, 4),
(30, 5),
(30, 6),
(30, 7),
(30, 8),
(31, 3),
(31, 4),
(31, 5),
(31, 6),
(31, 7),
(31, 8),
(32, 3),
(32, 4),
(32, 5),
(32, 6),
(32, 7),
(32, 8);

-- --------------------------------------------------------

--
-- Table structure for table `sizes`
--

CREATE TABLE `sizes` (
  `id` int(10) UNSIGNED NOT NULL,
  `size` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sizes`
--

INSERT INTO `sizes` (`id`, `size`) VALUES
(1, 36),
(2, 37),
(3, 38),
(4, 39),
(5, 40),
(6, 41),
(7, 42),
(8, 43),
(9, 44),
(10, 45),
(11, 46),
(12, 47),
(13, 48);

-- --------------------------------------------------------

--
-- Table structure for table `slides`
--

CREATE TABLE `slides` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slot` tinyint(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `slides`
--

INSERT INTO `slides` (`id`, `category_id`, `image`, `slot`) VALUES
(1, 1, 'http://hypershoes.test/images/94830bn1.jpg', 1),
(2, 3, 'http://hypershoes.test/images/48921bn2.png', 2),
(3, 2, 'http://hypershoes.test/images/1972693047SneakerHDWallpapers.com-Your-favorite-sneakers-in-HD-and-.jpg', 3);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` char(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` datetime DEFAULT NULL,
  `remember_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `password`, `email`, `phone`, `address`, `role`, `email_verified_at`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'NGUYEN VAN HAU', '$2y$10$IjH8rFFnPz1o7.r/UtGjIO03NhnNyCpefkJ5RUNhs6wjtJapMDEga', 'nevermorethuan1408@gmail.com', '0905332684', 'binh thanh, ho chi minh', 'admin', '2020-08-22 16:26:10', '', '2020-08-22 09:25:17', '2020-08-22 09:35:23'),
(2, 'NGUYEN VAN HAU', '$2y$10$PZyd2DMrGjwwJlgjvzQS/emYsG15hNWOS1gkzVnf1p1th./x2Meve', 'hau.nguyen@beehexa.com', '0905332684', 'tay son inahdas', NULL, '2020-08-23 07:19:50', NULL, '2020-08-23 00:19:34', '2020-08-23 00:20:19');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `guarantee`
--
ALTER TABLE `guarantee`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_type` (`id_type`);

--
-- Indexes for table `guarantee_type`
--
ALTER TABLE `guarantee_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_user_id_index` (`user_id`);

--
-- Indexes for table `order_product`
--
ALTER TABLE `order_product`
  ADD KEY `order_product_product_id_order_id_index` (`product_id`,`order_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_category_id_index` (`category_id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_images_product_id_index` (`product_id`);

--
-- Indexes for table `product_size`
--
ALTER TABLE `product_size`
  ADD KEY `product_size_product_id_size_id_index` (`product_id`,`size_id`),
  ADD KEY `size_id` (`size_id`);

--
-- Indexes for table `sizes`
--
ALTER TABLE `sizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slides`
--
ALTER TABLE `slides`
  ADD PRIMARY KEY (`id`),
  ADD KEY `slides_category_id_index` (`category_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `guarantee`
--
ALTER TABLE `guarantee`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `guarantee_type`
--
ALTER TABLE `guarantee_type`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=133;

--
-- AUTO_INCREMENT for table `sizes`
--
ALTER TABLE `sizes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `slides`
--
ALTER TABLE `slides`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `guarantee`
--
ALTER TABLE `guarantee`
  ADD CONSTRAINT `guarantee_ibfk_1` FOREIGN KEY (`id_type`) REFERENCES `guarantee_type` (`id`);

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `order_product`
--
ALTER TABLE `order_product`
  ADD CONSTRAINT `order_product_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  ADD CONSTRAINT `order_product_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`);

--
-- Constraints for table `product_images`
--
ALTER TABLE `product_images`
  ADD CONSTRAINT `product_images_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Constraints for table `product_size`
--
ALTER TABLE `product_size`
  ADD CONSTRAINT `product_size_ibfk_1` FOREIGN KEY (`size_id`) REFERENCES `sizes` (`id`),
  ADD CONSTRAINT `product_size_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Constraints for table `slides`
--
ALTER TABLE `slides`
  ADD CONSTRAINT `slides_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
