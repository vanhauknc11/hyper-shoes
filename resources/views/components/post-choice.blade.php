@if(!empty($postsChoice))
<high-lights>

    <div class="container">
        <div class="titlePrimary">
            Tạp chí thời trang 2020
            <p></p>
        </div>
        <div class="row rowWrap">
        	@foreach($postsChoice as $postsChoice)
            <div class="item">
                <p><a href="{{ $postsChoice->slug }}/post">{{ $postsChoice->title }}</a></p>
            </div>
            @endforeach
        </div>
    </div>
</high-lights>
@endif