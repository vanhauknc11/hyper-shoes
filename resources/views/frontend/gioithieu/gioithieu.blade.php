@extends('layouts.frontend.master')
@section('page_title', $post->title)
@section('main_content')
<div class="post">

<div class="container">
    <h3>{{$post->title}}</h3>

    
    <img src="{{url($post->image)}}" alt="">

    <p>{{$post->content}}</p>

</div>

</div>

@endsection