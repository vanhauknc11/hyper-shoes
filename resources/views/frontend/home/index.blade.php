@extends('layouts.frontend.master')
@section('page_title','Hyper Shoes')
@section('main_content')
@include('frontend.slide.slide',['slides'=>$slides ?? null])  
	  <x-shipping></x-shipping>
	<x-title title="- sản phẩm mới"></x-title>
	@include('frontend.products._list', ['products' => $products,'new'=>true])

	<x-title title="- dành cho bạn"></x-title>
	<x-slideproduct limit="30"></x-slideproduct>
	<x-title title="- sản phẩm giá đẹp"></x-title>
	<x-accessories limit="12"></x-accessories>
	<x-title title="- tin tức mới"></x-title>
	<x-news></x-news>
@endsection