<footer>
    <div class="copy">
        <div class="footer">
            <div class="row">
            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    <h5>THÔNG TIN LIÊN HỆ</h5>
                    <p>Địa chỉ: 192/2 Nguyễn Thái Bình, Phường 12, Quận Tân Bình, Thành phố Hồ Chí Minh</p>
                    <p>Email : cskh.kingshoes.vn@gmail.com</p>
                    <p>Hotline CSKH: 0902.368.001</p>
                </div>

                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                <h5>HỖ TRỢ KHÁCH HÀNG</h5>
                <p>Chăm sóc khách hàng</p>
                <p>Thánh toán</p>
                <p>Hướng dẫn mua hàng</p>
                </div>

                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                <h5>CHÍNH SÁCH</h5>
                <p>Chế độ bảo hành</p>
                <p>Bảo mật thông tin</p>
                <p>Chính sách giao nhận</p>
                </div>

                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                <h5>FANPAGE</h5>
                </div>
            
            </div>
            
            
        
        </div>
    </div>
</footer>
</body>
</html>