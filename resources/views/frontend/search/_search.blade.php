<searchx>
<div class="searchx container">
<label for="exampleFormControlInput1">Tìm kiếm sản phẩm</label>
    <!-- Let all your things have their places; let each part of your business have its time. - Benjamin Franklin -->
<form class="formx" action="{{url('search2')}}" method="GET">


<div class="input-group ">
  <select class="custom-select" id="inputGroupSelect02" name="size">
    <option selected>Size</option>
    @foreach($sizes as $size)
    <option value="{{$size->size}}">{{$size->size}}</option>
    @endforeach
    
    
  </select>
  
</div>

<div class="input-group">

  <select class="custom-select" id="inputGroupSelect02" name="range">
    <option selected>Khoảng giá</option>
    <option value="1">Dưới 3 Triệu</option>
    <option value="2">Từ 3 đến 5 Triệu</option>
    <option value="3">Trên 5 Triệu</option>
  </select>
  
</div>

<div class="input-group">

  <select class="custom-select" id="inputGroupSelect02" name="sort">
    <option value="asc">Giá thấp đến cao</option>
    <option value="desc">Giá cao đến thấp</option>
  </select>
  
</div>
<div class="input-group">
<button type="submit" class="btn btn-warning">Tìm Giày Ngay</button>
</div>
</form>

</div>
</searchx>