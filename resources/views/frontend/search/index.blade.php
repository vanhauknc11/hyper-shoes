@extends('layouts.frontend.master')

@section('page_title','Tìm kiếm')
@section('main_content')

	@include('frontend.products._list', ['productPaginate' => $products])


@endsection