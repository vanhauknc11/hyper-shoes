@extends('layouts.app')

@section('content')


<div class="container">
     
        <div class="row">
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
           
    
       
        
            <ul class="list-group" style="list-style: none;">
                <li> <a class="list-group-item list-group-item-action" href="{{route('user')}}"> Thông tin tài khoản</a> </li>
                <li><a class="list-group-item list-group-item-action" href="{{route('orders')}}"> Quản lý đơn hàng</a></li>
            </ul>
        </div>
        
        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
            <h3>Đơn hàng của tôi</h3>

            @if(Session::has('message'))
                <div class="alert alert-success">
                {{Session::get('message')}}
                </div>
                @endif
                <br>
                <table width="100%" class="table" cellspacing="0" cellpadding="0">
                <thead class="thead-dark">
                <tr height="45" align="center">

                    <td>Mã đơn hàng</td>
                    <td>Ngày mua</td>
                    <td>Sản phẩm</td>
                    <td>Tổng tiền</td>
                    <td>Trạng thái đơn hàng</td>
                
                </tr>
                </thead>
                @foreach($orders as $order)

                @if($order->status==0)
                <tr height="45" align="center" class="table-primary">
                @endif

                @if($order->status==1)
                <tr height="45" align="center" class="table-success">
                @endif

                @if($order->status==2)
                <tr height="45" align="center" class="table-danger">
                @endif
              

                    <td><a href="{{url('user/orders/view')}}/{{$order->order_id}}">{{$order->order_id}}</a> </td>
                    <td>{{$order->created_at}}</td>
                    <td>{{$order->SoSanPham}}</td>
                    <td>{{number_format($order->TongTien,0)}} VNĐ</td>
                    @if($order->status == 0)
                    <td> Chờ phê duyệt</td>
                    @elseif($order->status == -1)
                    <td> Đã hủy</td>
                    @else
                    <td> Thành Công</td>
                    @endif
                </tr>
                    
                @endforeach
                </table>

            
         
            </div>

            </div>
            <div class="pagenigate">
            {{$orders->links()}}  
            </div>
         
            </div>


     

@endsection