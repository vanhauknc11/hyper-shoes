@extends('layouts.app')

@section('content')




    <div class="container">
     
     <div class="row">
     <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
        

     <ul class="list-group" style="list-style: none;">
                <li> <a class="list-group-item list-group-item-action" href="{{route('user')}}"> Thông tin tài khoản</a> </li>
                <li><a class="list-group-item list-group-item-action" href="{{route('orders')}}"> Quản lý đơn hàng</a></li>
            </ul>
     </div>
     
     <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
         <h3>Chi tiết đơn hàng #{{$orders->id}} - Giao thành công</h3>

         @if(Session::has('message'))
             <div class="alert alert-success">
             {{Session::get('message')}}
             </div>
             @endif
             <br>

        <div class="info">
            <h3>{{$orders->name}}</h3>
            <p>Địa chỉ: {{Auth::user()->address}}</p>
            <p>Điện thoại: {{Auth::user()->phone}}</p>
            <p>Ghi chú : {{$orders->note}}</p>
            <p>Ngày đặt hàng: {{$orders->created_at}}</p>
        </div>
<br>
        <div class="dssanpham">
        @php
            $total = 0;
        @endphp

        <table width="100%" class="table table-dark table-striped" cellspacing="0" cellpadding="0" >
                <tr height="45" align="center">

                 
                    <td>Tên sản phẩm</td>
                    <td>Số lượng</td>
                    <td>Đơn giá</td>
                    <td>Giảm giá</td>
                    <td>Tạm tính</td>

             
                
                </tr>
     
                 @foreach($products as $product)
                        <tr height="45" align="center">
                            <td>{{$product->product_name}}</td>
                            <td>{{$product->qty}}</td>
                            <td>{{number_format($product->product_price,0)}} VNĐ</td>
                            <td>0 đ</td>
                            <td>{{number_format($product->total,0)}} VNĐ</td>
                            @php
                            $total += $product->total;
                        @endphp
                        
                        </tr>
                      @endforeach

                <tr>
               
                    <td height="30" align="right" colspan="5">
                        Tổng tiền : {{number_format($total,0)}} VNĐ
                    </td>
                </tr>
               
            
            </table>
         

        </div>

            
         


     </div>
     

     </div>
     
    
     </div>

   



@endsection
