@extends('layouts.app')

@section('content')





    <div class="container">
     
     <div class="row">
     <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
        
     <ul class="list-group" style="list-style: none;">
                <li> <a class="list-group-item list-group-item-action" href="{{route('user')}}"> Thông tin tài khoản</a> </li>
                <li><a class="list-group-item list-group-item-action" href="{{route('orders')}}"> Quản lý đơn hàng</a></li>
            </ul>
     </div>
     
     <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
         <h3>Thông tin tài khoản</h3>

         @if(Session::has('message'))
             <div class="alert alert-success">
             {{Session::get('message')}}
             </div>
             @endif
      
         <form action="" method="post">
         @csrf
             <div class="form-group">
                     <label >Họ tên</label>
                     <input type="text" class="form-control" name="name" placeholder="Nguyễn Văn A"  value="{{Auth::user()->name}}">
                   
                 </div>

                 <div class="form-group">
                     <label >Số điện thoại</label>
                     <input type="number" class="form-control" name="phone" value="{{Auth::user()->phone}}" >
                 </div>

                 <div class="form-group">
                     <label >Địa chỉ</label>
                     <input type="text" class="form-control" name="address" value="{{Auth::user()->address}}" >
                 </div>

                 <div class="form-group">
                     <label for="exampleInputEmail1">Địa chỉ email</label>
                     <input type="email" class="form-control"  name="email" value="{{Auth::user()->email}}" readonly="true" >
    
                 </div>

                 <label class="form-check-label" for="exampleCheck1">Thay đổi mật khẩu</label>

                 <div class="form-group form-check">
                     <input type="checkbox" class="form-check-input" name="checkbox" id="myCheck" onclick="myFunction()" >
                 </div>
                 <br>

                 <div class="pass" id="pass" style="display:none">

                 <div class="form-group">
                     <label >Mật khẩu cũ</label>
                     <input type="password" class="form-control" name="oldpassword" value="" >
                 </div>

                 <div class="form-group">
                     <label >Mật khẩu mới</label>
                     <input type="password" class="form-control" name="password" value="" >
                 </div>

                 <div class="form-group">
                     <label >Nhập lại</label>
                     <input type="password" class="form-control" name="repassword" value="" >
                 </div>

                   
                 </div>
                 <button type="submit" class="btn btn-primary">Cập nhật</button>
         </form>
         


     </div>
     

     </div>
     
    
     </div>

     <script>
     
     function myFunction() {
         // Get the checkbox
         var checkBox = document.getElementById("myCheck");
         // Get the output text
         var text = document.getElementById("pass");

         // If the checkbox is checked, display the output text
         if (checkBox.checked == true){
             text.style.display = "block";
         } else {
             text.style.display = "none";
         }
         }
     
     </script>



@endsection
