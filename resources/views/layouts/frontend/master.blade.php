<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('page_title')</title>
    <meta name="description" content="@yield('meta_desc')" />
    <meta name="keywords" content="@yield('meta_keyword')" />
    <link rel="stylesheet" href="{{ url('css/stylesheet.css') }}" />
    <link rel="stylesheet" href="{{ url('css/jquery.fancybox.css') }}" />
    <link rel="stylesheet" href="{{ url('css/apptow.css') }}" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css?family=Quicksand:400,500,600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://kit-pro.fontawesome.com/releases/v5.12.0/css/pro.min.css">
    <script src="{{ url('js/jquery-3.5.1.min.js') }}" ></script>
    <link rel="stylesheet" href="{{url('css/owlcarousel/owl.carousel.min.css')}}">
	<link rel="stylesheet" href="{{url('css/owlcarousel/owl.theme.default.min.css')}}">
    <script src="{{url('css/owlcarousel/owl.carousel.min.js')}}"></script>




@include('frontend.parttions.header')


@yield('main_content')

@include('frontend.parttions.footer')


<script type="text/javascript">
	
	$(document).ready(function(){

	  $(".owl-carousel").owlCarousel({


    items : 5,
    itemsDesktop : [1199,4],
    itemsDesktopSmall : [980,3],
    itemsTablet: [768,2],
    itemsTabletSmall: false,
    itemsMobile : [479,1],
    singleItem : false,
    margin:15,
      });

    
	});
</script>