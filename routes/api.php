<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::namespace('Auth\Api')->prefix('v1')->group(function(){
    Route::post('login','LoginController@login');
    Route::post('logout', 'LoginController@logout');
    Route::post('refresh', 'LoginController@refresh');
    Route::post('me', 'LoginController@me');
});
//middleware('auth:api')->
Route::namespace('Api')->prefix('v1')->group(function(){

    Route::resource('category','CategoryController')->except(['create','edit']);
    Route::get('categoryall','CategoryController@getAll');
    Route::resource('slide','SlideController')->except(['create','edit']);
    Route::resource('product','ProductController')->except(['create','edit']);
  //  Route::post('size/{id}','ProductController@addSize');

    Route::get('multiimage/{id}','MultiImageController@getAll');
    Route::post('multiimage','MultiImageController@createImage');
    Route::delete('multiimage/{id}','MultiImageController@destroyImage');
    Route::get('size/{id}','SizeController@getOne');
    Route::get('size','SizeController@getAll');
    Route::post('size/{id}','SizeController@addSize');

    Route::resource('post','PostController')->except(['create','edit']);

    Route::get('orders','OrderController@getAll');
    Route::put('orders/{id}','OrderController@processUpdate');
    Route::get('orders/{id}','OrderController@productData');

    Route::post('guarantee','GuaranteeController@getAll');
    Route::get('guarantee','GuaranteeController@getType');
    Route::post('guarantee1','GuaranteeController@addGua');
    Route::post('creategua','GuaranteeController@createType');

    Route::post('statistical','GuaranteeController@statistical');
});


